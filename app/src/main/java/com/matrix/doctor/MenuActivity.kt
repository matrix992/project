package com.matrix.doctor

import android.app.ActivityOptions
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.transition.Explode
import android.transition.Slide
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.animation.AnticipateOvershootInterpolator
import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        initTransition()

        window.allowEnterTransitionOverlap=false
    }

    public fun btnExamination(view: View){

        val activityOption= ActivityOptions.makeSceneTransitionAnimation(this,
            android.util.Pair(imgExamination,"img_examination_shared"),
            android.util.Pair(txtExamination,"txt_examination_shared"),
            android.util.Pair(txtGrid,"smarthed_shared"))

        val intent= Intent(this,AnalysisActivity::class.java)
        startActivity(intent,activityOption.toBundle())
    }
    fun initTransition(){
/*
                val enterTransition: Explode = Explode()
                enterTransition.duration = resources.getInteger(R.integer.anim_duration_long).toLong()
                window.enterTransition = enterTransition
*/
        val enterTransition= Slide()
        enterTransition.slideEdge= Gravity.LEFT
        enterTransition.duration=resources.getInteger(R.integer.anim_duration_long).toLong()
        enterTransition.interpolator= LinearInterpolator()
        window.enterTransition=enterTransition

        }

}
